<?php


namespace App\Services\Question;




use App\Http\Requests\QuestionRequest;
use App\Question;

interface QuestionInterface
{

    public function store(QuestionRequest $request);
    public function update(QuestionRequest $request, Question $question);
}
