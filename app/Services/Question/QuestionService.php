<?php


namespace App\Services\Question;


use App\Http\Controllers\Controller;
use App\Http\Requests\QuestionRequest;
use App\Question;

class QuestionService extends Controller implements QuestionInterface
{

    public function store(QuestionRequest $request)
    {
        $question = Question::create($request->all());
        $question->user()->associate(auth()->user())->save();
        return $question;
    }

    public function update(QuestionRequest $request, Question $question)
    {
        $question =$question->update($request->all());
        return $question;
    }
}
