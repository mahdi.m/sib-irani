<?php


namespace App\Services\Answer;


use App\Answer;
use App\Http\Requests\AnswerRequest;

interface AnswerInterface
{
    public function store(AnswerRequest $request);
    public function update(AnswerRequest $request, Answer $answer);
}
