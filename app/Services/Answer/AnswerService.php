<?php


namespace App\Services\Answer;


use App\Answer;
use App\Http\Controllers\Controller;
use App\Http\Requests\AnswerRequest;

class AnswerService extends Controller implements AnswerInterface
{

    public function store(AnswerRequest $request)
    {
        $answer = Answer::create($request->all());
        $answer->user()->associate(auth()->user())->save();
        $answer->question()->associate($request->question_id)->save();
        return $answer;
        // TODO: Implement store() method.
    }

    public function update(AnswerRequest $request, Answer $answer)
    {
        $answer =$answer->update($request->all());
        return $answer;
        // TODO: Implement update() method.
    }
}
