<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\HomeController::class, 'index']);


Auth::routes();
Route::get('login', [\App\Http\Controllers\Auth\LoginController::class,'showLoginForm'])->name('login');
Route::post('login', [\App\Http\Controllers\Auth\LoginController::class,'login']);
Route::get('logout', [\App\Http\Controllers\Auth\LoginController::class,'logout'])->name('logout');

Route::middleware('auth')->prefix('profile')->namespace('panel')->group(function () {
    Route::get('/', [\App\Http\Controllers\UserController::class, 'index'])->name('profile');
    Route::resource('questions', 'QuestionController');
});
Route::resource('questions', 'Web\QuestionController', ['as' => 'web']);
Route::resource('answers', 'Web\AnswerController', ['as' => 'web']);

