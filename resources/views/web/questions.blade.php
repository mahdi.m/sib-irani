@extends('layouts.app')

@section('content')
    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper">
                <div class="container-fluid">
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="card-title-wrap bar-success">
                                            <h4 class="card-title">همه سوالات</h4>
                                        </div>
                                    </div>
                                    <div class="card-body collapse show">
                                        <div class="card-block card-dashboard">
                                            <table class="table table-striped table-bordered zero-configuration">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>متن سوال</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach($questions as $key => $question)
                                                        <tr>
                                                            <td>{{ $key }}</td>
                                                            <td>
                                                                <a href="{{ route('web.questions.show', ['question' => $question->id]) }}">
                                                                    {{ $question->title }}
                                                                </a>
                                                            </td>
                                                        </tr>
                                                @endforeach
                                                </tbody>
{{--                                                <tfoot>--}}
{{--                                                <tr>--}}
{{--                                                    <th>نام</th>--}}
{{--                                                    <th>موقعیت</th>--}}
{{--                                                    <th>اداره</th>--}}
{{--                                                    <th>سن</th>--}}
{{--                                                    <th>تاریخ شروع</th>--}}
{{--                                                    <th>حقوق</th>--}}
{{--                                                </tr>--}}
{{--                                                </tfoot>--}}
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

@endsection
