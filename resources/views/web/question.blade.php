@extends('layouts.app')


@section('css')
    <link rel="stylesheet" href="{{ asset('student-panel/css/app.css') }}">
@endsection

@section('content')

    <div class="main-panel rtl">
        <div class="main-content">
            <div class="content-wrapper">
                <div class="container-fluid">
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="card-title-wrap bar-success">
                                            <h4 class="card-title font-weight-bold">{{ $question->title }}</h4>
                                        </div>
                                    </div>
                                    <div class="card-body collapse show">
                                        <div class="card-block card-dashboard">
                                            <p>
                                                {{ $question->desc }}
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body collapse show bg-eee">
                                    <div class="card-block card-dashboard">
                                        @foreach($question->answers as $answer)
                                            <div class="card p-3">
                                                <div class="card-body">
                                                    <h5 class="card-title">{{ $answer->user->name }}</h5>
                                                    <p class="card-text">{{ $answer->desc }}</p>
                                                    {{--                                                    <a href="#" class="btn btn-primary">Button</a>--}}
                                                </div>
                                            </div>
                                        @endforeach

                                        @guest
                                            <div class="text-center bg-grey p-1">
                                                برای ارسال پاسخ باید
                                                <a href="{{ route('login') }}" ><strong class="text-danger"> وارد </strong></a>
                                                شوید
                                            </div>
                                        @else
                                            <form class="form form-horizontal" action="{{ route('web.answers.store') }}" method="post">
                                                @csrf
                                                <input type="hidden" name="question_id" value="{{ $question->id }}">
                                                <div class="form-body">

                                                    <div class="form-group row text">
                                                        <label class="col-md-3 label-control" for="projectinput9">
                                                            پاسخ شما :
                                                        </label>
                                                        <div class="col-md-6">
                                                            <textarea id="projectinput9" rows="10" class="form-control"
                                                                      name="desc"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-3" >

                                                        </div>
                                                        <div class="col-md-6 text-center" for="projectinput9">
                                                            <button type="submit" class="btn btn-success green-gradient  w-25">
                                                                ارسال پاسخ
                                                            </button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </form>
                                        @endguest

                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

@endsection
