@extends('panel.master-new')


@section('css')
    <meta name="yn-tag" id="070d272d-fd60-452c-860c-1fa6971d609e">
    <link href="{{ asset('css/bootstrap-tour.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/student-panel.css') }}" type="text/css">
@endsection
@section('content')
    <div class="main-panel bg-white">
        <div class="main-content">
            <div class="content-wrapper mt-4">
                <div class="container-fluid">
                    <section id="grid">

                    </section>
                </div>
            </div>
        </div>
    </div>




@endsection
