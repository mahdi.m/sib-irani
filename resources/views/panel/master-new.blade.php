<!DOCTYPE html>
<html lang="en" class="loading">

<!-- Mirrored from pixinvent.com/demo/convex-bootstrap-admin-dashboard-template/demo-1/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Sep 2018 14:53:24 GMT -->

<!-- Mirrored from logicgroup.ir/convex/convex-demos/demo-4/app-assets/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Feb 2019 13:05:04 GMT -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content=" admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template,  admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>سیب ایرانی</title>
    <link rel="apple-touch-icon" sizes="60x60" href="img/ico/apple-icon-60.html">
    <link rel="apple-touch-icon" sizes="76x76" href="img/ico/apple-icon-76.html">
    <link rel="apple-touch-icon" sizes="120x120" href="img/ico/apple-icon-120.html">
    <link rel="apple-touch-icon" sizes="152x152" href="img/ico/apple-icon-152.html">
    <link rel="shortcut icon" type="image/x-icon" href="https://pixinvent.com/demo/convex-bootstrap-admin-dashboard-template/app-assets/img/ico/favicon.ico">
    <link rel="shortcut icon" type="image/png" href="img/ico/favicon-32.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900%7CMontserrat:300,400,500,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('student-panel/fonts/feather/style.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('student-panel/fonts/simple-line-icons/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('student-panel/fonts/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('student-panel/css/perfect-scrollbar.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('student-panel/css/prism.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('student-panel/css/chartist.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('student-panel/css/app.css') }}">
</head>
<body data-col="2-columns" class=" 2-columns ">
<!-- ////////////////////////////////////////////////////////////////////////////-->
<div class="wrapper">
@include('panel.navigation-new')
<!-- right side column. contains the logo and sidebar -->

    @include('panel.rightSide-new')

    <div class="wrapper">
            @yield('content')
    </div>
</div>
<!-- BEGIN VENDOR JS-->
<script src="{{ asset('student-panel/js/jquery-3.3.1.min.js') }}"></script>
{{--<script src="js/persian-datepicker.min.html"></script>--}}
<script src="{{ asset('student-panel/js/popper.min.js') }}"></script>
<script src="{{ asset('student-panel/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('student-panel/js/perfect-scrollbar.jquery.min.js') }}"></script>
<script src="{{ asset('student-panel/js/prism.min.js') }}"></script>
<script src="{{ asset('student-panel/js/jquery.matchHeight-min.js') }}"></script>
<script src="{{ asset('student-panel/js/screenfull.min.js') }}"></script>
<script src="{{ asset('student-panel/js/pace.min.js') }}"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{ asset('student-panel/js/chartist.min.js') }}"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN CONVEX JS-->
<script src="{{ asset('student-panel/js/app-sidebar.js') }}"></script>
<script src="{{ asset('student-panel/js/notification-sidebar.js') }}"></script>
<script src="{{ asset('student-panel/js/customizer.js') }}"></script>
<!-- END CONVEX JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="{{ asset('student-panel/js/dashboard-ecommerce.js') }}"></script>
<!-- END PAGE LEVEL JS-->
</body>
</html>
