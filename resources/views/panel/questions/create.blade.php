@extends('panel.master-new')


@section('css')
    <meta name="yn-tag" id="070d272d-fd60-452c-860c-1fa6971d609e">
    <link href="{{ asset('css/bootstrap-tour.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/student-panel.css') }}" type="text/css">
@endsection
@section('content')

    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper">
                <div class="container-fluid">
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="card-title-wrap bar-success">
                                            <h4 class="card-title">سوالات من</h4>
                                        </div>
                                    </div>
                                    <div class="card-body collapse show">
                                        <div class="card-block card-dashboard">
                                            <form class="form form-horizontal" action="{{ route('questions.store') }}" method="post">
                                                @csrf
                                                <div class="form-body">

                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput1">عنوان سوال
                                                            : </label>
                                                        <div class="col-md-6">
                                                            <input type="text" id="projectinput1" class="form-control"
                                                                   name="title" >
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput9">
                                                            متن سوال :
                                                        </label>
                                                        <div class="col-md-6">
                                                            <textarea id="projectinput9" rows="10" class="form-control"
                                                                      name="desc"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-3" >

                                                        </div>
                                                        <div class="col-md-6 text-center" for="projectinput9">
                                                            <button type="submit" class="btn btn-success green-gradient  w-75">
                                                                ذخیره تغییرات
                                                            </button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

@endsection
