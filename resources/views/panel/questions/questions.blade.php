@extends('panel.master-new')


@section('css')
    <meta name="yn-tag" id="070d272d-fd60-452c-860c-1fa6971d609e">
    <link href="{{ asset('css/bootstrap-tour.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/student-panel.css') }}" type="text/css">
@endsection
@section('content')

    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper">
                <div class="container-fluid">
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="card-title-wrap bar-success">
                                            <h4 class="card-title">سوالات من</h4>
                                        </div>
                                    </div>
                                    <div class="card-body collapse show">
                                        <div class="card-block card-dashboard">
                                            <a href="{{ route('questions.create') }}" class="btn mr-1 btn-success">ایجاد سوال</a>
                                            {{--                                            <p class="card-text">داده های جداول اغلب ویژگی های پیش فرض را فعال می کنند، بنابراین همه چیزهایی که باید برای استفاده از آن با استفاده از ables خود انجام دهید، تماس با تابع ساخت است: $ (). DataTable ()؛</p>--}}
                                            <table class="table table-striped table-bordered zero-configuration">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>عنوان سوال</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach($questions as $key => $question)
                                                    <tr>
                                                        <td>{{ $key }}</td>
                                                        <td>
                                                            <a href="{{ route('web.questions.show', ['question' => $question->id]) }}">{{ $question->title }}</a>
                                                        </td>

                                                    </tr>
                                                @endforeach
                                                </tbody>
{{--                                                <tfoot>--}}
{{--                                                <tr>--}}
{{--                                                    <th>نام</th>--}}
{{--                                                    <th>موقعیت</th>--}}
{{--                                                    <th>اداره</th>--}}
{{--                                                    <th>سن</th>--}}
{{--                                                    <th>تاریخ شروع</th>--}}
{{--                                                    <th>حقوق</th>--}}
{{--                                                </tr>--}}
{{--                                                </tfoot>--}}
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

@endsection
