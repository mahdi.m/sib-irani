<div data-active-color="white" data-background-color="crystal-clear" data-image="../app-assets/img/sidebar-bg/08.jpg"
     class="app-sidebar">
    <div class="sidebar-header">
        <div class="logo clearfix">
            <div class="border-bottom pt-2 pb-2 pr-2">
                <a id="sidebarClose" href="javascript:;" class="nav-close d-block d-md-block d-lg-none d-xl-none">
                    <i class="ft-circle"></i>
                </a>
                <a href="index-2.html" class="logo-text text-right pr-2">
                    <span class="text align-middle font-medium-1 menu-title">{{ auth()->user()->name }}</span></a>
                <a id="sidebarToggle" href="javascript:;" class="nav-toggle d-none d-sm-none d-md-none d-lg-block ">
                    <i data-toggle="expanded" class="fa fa-bars toggle-icon"></i>
                </a>

            </div>
            <div class="logo-img w-100">

                <div class="text-center pt-2">
                                <span class="avatar avatar-online img-circle-50 p-1">

                            <p class="d-none">تنظیمات کاربر</p>
                            </span>
                    <div class="mt-1 border-bottom pb-3 ">
                        <strong class="text-white font-medium-1 profile-name menu-title">
                        </strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar-content">
        <div class="nav-container">
            <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
                <li class=" nav-item">
                    <a href="{{ route('questions.index') }}">
                        <i class="icon-layers"></i>
                        <span data-i18n="" class="menu-title">سوالات من</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="sidebar-background"></div>
</div>
