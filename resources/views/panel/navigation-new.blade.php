<nav class="navbar navbar-expand-lg navbar-light bg-faded">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" data-toggle="collapse" class="navbar-toggle d-lg-none float-right"><span
                    class="sr-only">تغییر ناوبری </span><span class="icon-bar"></span><span
                    class="icon-bar"></span><span class="icon-bar"></span></button>
            <span class="d-lg-none navbar-right navbar-collapse-toggle"><a class="open-navbar-container"><i class="ft-more-vertical"></i></a></span>

            <div class="dropdown mr-2 display-inline-block">
                <img class="img-60-60" src="/admin/dist/img/TK PR LOGO.png" alt="">
            </div>
        </div>
        <div class="navbar-container">
            <div id="navbarSupportedContent" class="collapse navbar-collapse">
                <ul class="navbar-nav ml-2">

                    <li class="nav-item pt-2">
                        <a class="nav-link green-gradient" href="{{ route('logout') }}" id="teacher-request">
                            <span>خروج</span>
                        </a>
                    </li>


                </ul>
            </div>
        </div>
    </div>
</nav>
